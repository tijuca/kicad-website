+++
title = "Thank you for supporting KiCad!"
date = "2021-03-26"
aliases = [ "/thank-you/" ]
headless = true
summary = "Thank you for your donation"
+++

{{< aboutlink "/img/about/kicad-logo.png" "/img/kicad_logo_paths.svg" >}}


KiCad is user-driven and user-supported.  Your donation helps us to ensure that KiCad
development continues to improve.

